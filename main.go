package main

import (
	"fmt"

	"github.com/leekchan/accounting"
	"gitlab.com/freelancer-cuong/upwork-test/nah"
)

func main() {
	fmt.Println("hello world")

	var email = "hello@gmail.com"

	fmt.Println(email)

	ac := accounting.Accounting{Symbol: "$", Precision: 2}
	fmt.Println(ac.FormatMoney(123456789.213123))

	fmt.Println("Add of 4 and 5 is ", nah.Add(4, 5))
}
